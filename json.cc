//	Copyright (c) 2021 Matt Samudio (Albert Lea Data)  All Rights Reserved.
//	Contact information for Albert Lea Data is available at
//	http://www.albertleadata.com
//
//	This file is part of libjsmn.
//
//	libjsmn is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as
//	published by the Free Software Foundation, either version 3 of
//	the License, or (at your option) any later version.
//
//	libjsmn is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the
//	GNU Lesser General Public License along with libjsmn.
//	If not, see <http://www.gnu.org/licenses/>.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "jsmn.h"
#include "json.h"

char *strgen(const char *);

class MapNode : public JNode {
public:
	MapNode(nodetype);
	MapNode(const MapNode &);
	virtual ~MapNode();
	operator char *();
	nodetype type() { return(nt); };
	long value() { return(numeric); };
	char *key() { return(name); };
	int keys(char ***);
	JNode *get(char *);
	void set(char *,JNode *);

protected:
	nodetype nt;
	char *name;
	long numeric;
	char *txt;
	MapNode *child;
	MapNode *next;
	friend class JDoc;
};

class Str {
public:
	Str() { str=""; len=0; pgs=0; };
	virtual ~Str() { if (str) delete[] str; str=0L; };
	virtual operator char *() { return(str); };
	int append(char *);
protected:
	int len;
	int pgs;
	char *str;
};

class JDoc {
public:
	JDoc();
	virtual ~JDoc();
	JNode *parse(char *);
	long loaded() { return(nodes); };
protected:
	MapNode *walk(char *,char *);
	void chain(MapNode &,int,char *);
	void dump(MapNode &, Str &);
	friend class JSON;
protected:
	jsmn_parser p;
	jsmntok_t *t;
	long nodes;
	int tkn;
};

int Str::append(char *in) {
	int newlen=0;
	int inlen = in ? strlen(in) : 0;
	if (inlen) {
		newlen = len + inlen;
		int grow;
		for ( grow=0; newlen >= ((pgs+grow)*4096); grow++) { }
		if (grow) {
			pgs += grow;
			char *tmp = new char[pgs*4096];
			memset(tmp,0,pgs*4096);
			strcpy(tmp,str);
			if (len) delete[] str;
			str = tmp;
		}
		strcat(str,in);
		len = newlen;
	}
	return(newlen);
}

char *strgen(const char *txt) {
	int len = txt ? strlen(txt) : 0;
	char *copy = new char[len+1];
	memset(copy,0,len+1);
	strcpy(copy,(txt?txt:""));
	return(copy);
}

MapNode::MapNode(nodetype ntype) {
	nt = ntype;
	name=0L;
	numeric=0L;
//	txt = new char[16]; memset(txt,0,16);
	txt=0L;
	child=0L;
	next=0L;
}

MapNode::MapNode(const MapNode &node) : MapNode(node.nt) {
	numeric = node.numeric;
	if (node.name) { if (name) delete[] name; name = strgen(node.name); }
	if (node.txt) { if (txt) delete[] txt; txt = strgen(node.txt); }
}

MapNode::~MapNode() {
	if (name) delete[] name; name=0L;
	if (txt) delete[] txt; txt=0L;
	if (child) delete child; child=0L;
	if (next) delete next; next=0L;
	return;
}

MapNode::operator char *() {
	switch (nt) {
		case INT:
			if (!txt) txt=strgen("xxxxxxxxxxxxxxxx");
			sprintf(txt,"%ld",numeric);
			break;
		case BTF:
			if (!txt) txt=strgen("xxxxxxxxxxxxxxxx");
			sprintf(txt,"%s",(numeric?"true":"false"));
			break;
	}
	return(txt);
}

int MapNode::keys(char ***keymap) {
	int children=0;
	for (MapNode *node=child; node; node=node->next) children++;
	*keymap = new char *[children];
	memset(*keymap,0,sizeof(char *)*children);
	char **map = *keymap;
	int i=0;
	for (MapNode *node=child; node; node=node->next) map[i++] = strgen(node->name);
	return(children);
}

JNode *MapNode::get(char *nodekey) {
	JNode *found=0L;
	for (MapNode *node=child; node && !found; node=node->next) {
		if ( node->name && !strcmp(nodekey,node->name)) found=(JNode *)node;
	}
	return(found);
}

void MapNode::set(char *newkey,JNode *input) {
	MapNode *in = (MapNode *)input;
	MapNode *node=0L;
	MapNode *last=0L;
	MapNode *found=0L;
	for ( node=child; node; node=node->next) {
		if ( !strcmp(newkey,name)) {
		//	fprintf(stderr,"DBG=>>> replacing found node for %s\n",newkey);
			found=node;
		}
		last=node;
	}
	if (found) {
		node = found;
		node->nt = in->type();
		switch (node->nt) {
			case INT:
			case BTF:
				node->numeric = in->value(); break;
				break;
			case OBJ:
			case ARY:
			case STR:
				if (node->txt) delete[] node->txt;
				node->txt = strgen((char *)in);
				break;
		}
	} else {
		node = in;
		node->name = strgen(newkey);
	}
	if (!found) {
		if (last) {
		//	fprintf(stderr,"DBG=>>> appended new node for %s\n",newkey);
			last->next=node;
		} else {
		//	fprintf(stderr,"DBG=>>> initializing with new node for %s\n",newkey);
			child=node;
		}
	}
	return;
}

void mapNodeType( char *out, JNode::nodetype type) {
	switch (type) {
		case JNode::STR: strcpy(out,"STR"); break;
		case JNode::INT: strcpy(out,"INT"); break;
		case JNode::BTF: strcpy(out,"BTF"); break;
		case JNode::OBJ: strcpy(out,"OBJ"); break;
		case JNode::ARY: strcpy(out,"ARY"); break;
		default: strcpy(out,"???"); break;
	}
	return;
}

void mapTokenType( char *out, int type) {
	switch (type) {
		case JSMN_OBJECT: strcpy(out,"JSMN_OBJECT"); break;
		case JSMN_ARRAY: strcpy(out,"JSMN_ARRAY"); break;
		case JSMN_STRING: strcpy(out,"JSMN_STRING"); break;
		case JSMN_PRIMITIVE: strcpy(out,"JSMN_PRIMITIVE"); break;
		case JSMN_UNDEFINED: strcpy(out,"JSMN_UNDEFINED"); break;
		default: strcpy(out,"?"); break;
	}
	return;
}

char *parseTkn( jsmntok_t *tkn, char *txt) {
	int keylen = tkn->end - tkn->start;
	char *sKey = new char[keylen+1];
	memset(sKey,0,keylen+1);
	memcpy(sKey,&(txt[tkn->start]),keylen);
	return(sKey);
}

JDoc::JDoc() {
	t=0L;
	nodes=0L;
	tkn=0;
}

JDoc::~JDoc() {
	if (t) delete[] t; t=0L;
}

void JDoc::chain( MapNode &node, int child, char *json) {
	MapNode *base = (MapNode *)(&node);
	char sType[16];
	memset(sType,0,16);
//	...
	int swallowed=0;
	int ok=1;
	jsmntype_t nt = t[tkn].type;
	char *key = parseTkn(&(t[tkn]),json);
	char *txt=0L;
	if ( base->nt != JNode::ARY) {
		if (nt == JSMN_STRING) {
			int sz = t[tkn].size;
		//	fprintf(stderr,"DBG=>>> %03d: %s\n",tkn,"advancing for LABEL");
			tkn++;
			txt = parseTkn(&(t[tkn]),json);
			if (t[tkn].type == JSMN_STRING) {
				swallowed = 1;
			} else nt = t[tkn].type;
		} else if (nt == JSMN_OBJECT) {
		//	fprintf(stderr,"DBG=>>> %s: %s\n","Hit unlabeled OBJECT outside array",txt);
		} else if (nt == JSMN_PRIMITIVE) {
		//	fprintf(stderr,"DBG=>>> %s: %s\n","Hit unlabeled PRIMITIVE outside array",txt);
		} // else ok=0;
	} else {
		txt = key;
		key = strgen("xxxxxxxxxxx");
		sprintf(key,"%d",child);
		if (nt == JSMN_STRING) {
			swallowed = 1;
		} else if (nt == JSMN_OBJECT) {
		//	fprintf(stderr,"DBG=>>> %s: %s\n","Hit OBJECT inside array",txt);
		} // else fprintf(stderr,"DBG=>>> %s: %s\n","Hit non-string inside array",txt);
	}
//	...
	mapTokenType(sType,nt);
	MapNode *newnode=0L;
//	fprintf(stderr,"DBG=>>> [%03d]: %s -> %s\n",tkn,sType,key);
	if (ok) {
		switch (t[tkn].type) {
			case JSMN_OBJECT:
			//	fprintf(stderr,"DBG=>>> adding object walk for child %d (%s)\n",child,key);
				base->set(key,walk(key,json));
				break;
			case JSMN_ARRAY:
			//	fprintf(stderr,"DBG=>>> adding array walk for child %d (%s)\n",child,key);
				base->set(key,walk(key,json));
				break;
			case JSMN_STRING:
				if (swallowed) {
					newnode = new MapNode(JNode::STR);
					newnode->txt = strgen(txt);
					base->set(key,newnode);
				//	fprintf(stderr,"DBG=>>> adding %s/%s for child %d\n",key,txt,child);
				} // else fprintf(stderr,"DBG=>>> skipping %s/%s for child %d\n",key,txt,child);
				tkn++;
				break;
			case JSMN_PRIMITIVE:
			//	fprintf(stderr,"DBG=>>> adding primitive %s/%s for child %d\n",key,txt,child);
				if ((txt[0] == 't') || (txt[0] == 'f')) {
					newnode = new MapNode(JNode::BTF);
					newnode->numeric = (txt[0]=='t') ? 1 : 0;
					base->set(key,newnode);
				} else if ((txt[0] == 'n') || (txt[0] == 'N')) {
					newnode = new MapNode(JNode::STR);
					newnode->txt = strgen("null");
					base->set(key,newnode);
				} else {
					newnode = new MapNode(JNode::INT);
					newnode->numeric = strtol(txt,0L,10);
					base->set(key,newnode);
				}
				tkn++;
				break;
			case JSMN_UNDEFINED: strcpy(sType,"JSMN_UNDEFINED"); break;
			default: break;
		}
	} // else fprintf(stderr,"ERR=>>> %s\n","attempt to chain with no key");
	if (key) delete[] key; key=0L;
	if (txt) delete[] txt; txt=0L;
	return;
}

MapNode *JDoc::walk(char *key,char *json) {
	MapNode *base=0L;
	char sType[16];
	memset(sType,0,16);
	JNode::nodetype nt = JNode::OBJ;
	if (t[tkn].type == JSMN_ARRAY) {
		nt = JNode::ARY;
		strcpy(sType,"JSMN_ARRAY");
	} else strcpy(sType,"JSMN_OBJECT");
	int children = t[tkn].size;
//	...
	if ( sType[0] && children) {
		base = new MapNode(nt);
		base->name = key;
		base->txt = parseTkn(&(t[tkn]),json);
	//	fprintf(stderr,"NFO=>>> [%d:%d] %s: %s\n",tkn,children,sType,key);
		tkn++;
		for (int i=0; i < children; i++) chain(*base,i,json);
	} // else fprintf(stderr,"ERR=>>> walk on non-OBJ/ARY or empty node\n");
	return(base);
}

JNode *JDoc::parse(char *json) {
//	Ephermeral pass to detect token count
	jsmn_init(&p);
	int txtlen=strlen(json);
	int tokens = jsmn_parse(&p,json,txtlen,0L,0);
//	Token allocation
	t = new jsmntok_t[tokens];
	memset(t,0,sizeof(jsmntok_t)*tokens);
//	Actual parse with sufficient token allocation
	jsmn_init(&p);
	int r = jsmn_parse(&p,json,txtlen,t,tokens);
//	Dump tokens for diagnostic purposes ...
/*	char stt[16];
	for (int dbgtkn=0; dbgtkn < tokens; dbgtkn++) {
		memset(stt,0,16);
		mapTokenType(stt,t[dbgtkn].type);
		char *txt = parseTkn(&(t[dbgtkn]),json);
		fprintf(stderr,"DBG=>>> [%03d] %s: %s\n",dbgtkn,stt,txt);
		delete[] txt; txt=0L;
	}
	fprintf(stderr,"DBG=>>> =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n"); */
//	Walk tokens to populate map
	int ttt = t[0].type;
	int nt = ((ttt == JSMN_OBJECT) || (ttt == JSMN_ARRAY)) ? ttt : -1;
	JNode *root = (nt >= 0) ? (JNode *)(walk(strgen("root"),json)) : 0L;
	delete[] t; t=0L;
	return(root);
}

void JDoc::dump(MapNode &base, Str &json) {
	char **keys=0L;
	int children = base.keys(&keys);
	char *indent="";
	char *k = base.key();
	if (base.nt == JNode::OBJ) {
		json.append("{");
	} else if (base.nt == JNode::ARY) json.append("[");
	char *sDlm="";
	char tmp[2048];
	for (int i=0; i < children; i++) {
		char *key = keys[i];
		MapNode *current = (MapNode *)(base.get(key));
		if (current) {
			memset(tmp,0,2048);
			if ((current->nt != JNode::OBJ) && (current->nt != JNode::ARY)) {
				const char *sFmt = (current->nt == JNode::STR) ? "%s\"%s\":\"%s\"" : "%s\"%s\":%s";
				const char *sFmtA = (current->nt == JNode::STR) ? "%s\"%s\"" : "%s%s";
				if (base.nt == JNode::ARY) {
					sprintf(tmp,sFmtA,sDlm,(char *)(*current));
				} else sprintf(tmp,sFmt,sDlm,current->key(),(char *)(*current));
				json.append(tmp);
			} else {
				if (base.nt != JNode::ARY) {
					sprintf(tmp,"%s\"%s\":",sDlm,current->key());
				} else sprintf(tmp,"%s",sDlm);
				json.append(tmp);
				dump(*current, json);
			}
		}
		sDlm=",";
	}
	if (base.nt == JNode::OBJ) {
		json.append("}");
	} else if (base.nt == JNode::ARY) {
		json.append("]");
	}
	for (int i=0; i < children; i++) delete[] keys[i];
	if (keys) delete[] keys; keys=0L;
	return;
}

JSON::JSON() {
	root = new MapNode(JNode::OBJ);
}

JSON::~JSON() {
	if (root) delete root; root=0L;
}

long JSON::load(char *txt) {
	long loaded=0L;
	jsmn_parser p;
	if (root) delete root;
	JDoc *doc = new JDoc();
	root = doc->parse(txt);
	delete doc; doc=0L;
	return(loaded);
}

char *JSON::dump() {
	char *json=0L;
	JDoc doc;
	Str txt;
	if (root) doc.dump(*((MapNode *)root),txt);
	json = strgen((char *)txt);
	return(json);
}

JNode *JSON::get(char *path) {
	JNode *found=0L;
	char dlm='.';
	if (path && path[0]) {
		char uri[strlen(path)+1];
		strcpy(uri,path);
		char **tokens=0L;
		int cnt = (*uri == dlm) ? 0 : 1;
		for (char *c=uri; *c; c++) if (*c == dlm) cnt++;
		tokens = new char *[cnt+1]; memset(tokens,0,sizeof(char *)*(cnt+1));
		int idx=0;
		char *mrk=uri;
		for (char *c=uri; *c; c++) {
			if (*c == dlm) {
				*c = '\0';
				int len = strlen(mrk);
				tokens[idx] = new char[len+1]; memset(tokens[idx],0,len+1);
				strcpy(tokens[idx++],mrk);
				mrk=c; mrk++;
			}
		}
		if (*mrk) {
			int len = strlen(mrk);
			tokens[idx] = new char[len+1]; memset(tokens[idx],0,len+1);
			strcpy(tokens[idx],mrk);
		}
		MapNode *node = (MapNode *)root;
		for (int i=0; node && tokens[i]; i++) {
			node = (MapNode *)(node->get(tokens[i]));
		}
		found=node;
		for (int i=0; tokens[i]; i++) delete[] tokens[i];
		delete[] tokens;
	} else found=root;
	return(found);
}
