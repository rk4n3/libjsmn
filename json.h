//	Copyright (c) 2021 Matt Samudio (Albert Lea Data)  All Rights Reserved.
//	Contact information for Albert Lea Data is available at
//	http://www.albertleadata.com
//
//	This file is part of libjsmn.
//
//	libjsmn is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as
//	published by the Free Software Foundation, either version 3 of
//	the License, or (at your option) any later version.
//
//	libjsmn is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the
//	GNU Lesser General Public License along with libjsmn.
//	If not, see <http://www.gnu.org/licenses/>.
#ifndef _JSON_H_
#define _JSON_H_

class JSON;

class JNode {
public:
	enum nodetype { STR, INT, BTF, OBJ, ARY };
public:
	virtual operator char *()=0;
	virtual nodetype type()=0;
	virtual char *key()=0;
	virtual int keys(char ***)=0;
	virtual JNode *get(char *)=0;
	virtual void set(char *,JNode *)=0;
	virtual long value()=0;
protected:
	JNode() {};
	virtual ~JNode() {};
	friend class JSON;
};

class JSON {
public:
	JSON();
	virtual ~JSON();
	long load(char *);
	char *dump(void);
	JNode *get(char *path=0L);
protected:
	JNode *root;
};

#endif
