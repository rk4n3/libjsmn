#	Copyright (c) 2021 Matt Samudio (Albert Lea Data)  All Rights Reserved.
#	Contact information for Albert Lea Data is available at
#	http://www.albertleadata.com
#
#	This file is part of libjsmn.
#
#	libjsmn is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Lesser General Public License as
#	published by the Free Software Foundation, either version 3 of
#	the License, or (at your option) any later version.
#
#	libjsmn is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Lesser General Public License for more details.
#
#	You should have received a copy of the
#	GNU Lesser General Public License along with libjsmn.
#	If not, see <http://www.gnu.org/licenses/>.
LPX = 64
PGM = json
PGM_SRC = pgm.cc
LIB = jsmn
LIB_SRC = json.cc

INC = -I$(HOME)/include
#LDP = -L$(HOME)/lib -L/usr/lib/x86_64-linux-gnu
LDP = -L$(HOME)/lib

CFS = -g -c -fPIC $(INC)

all: $(PGM)

clean:
	@rm -fv $(PGM) lib$(LIB).so
	@rm -fv $(LIB_SRC:.cc=.o) $(PGM_SRC:.cc=.o)
	@rm -fv jsmn.h

pkg: lib$(LIB).so
	echo -e "\nNFO=>>> no packaging yet"

rls: lib$(LIB).so
	echo -e "\nNFO=>>> no release automation yet"

$(PGM): lib$(LIB).so $(PGM_SRC:.cc=.o)
	g++ -o $@ -g -fPIC $(PGM_SRC:.cc=.o) -L. -l$(LIB)

lib$(LIB).so: jsmn.h $(LIB_SRC:.cc=.o)
	g++ -g -fPIC -shared -o $@ -L. -lzmq $^

jsmn.h:
	@curl -O https://raw.githubusercontent.com/zserge/jsmn/master/jsmn.h

%.o: %.cc
	g++ $(CFS) -o $*.o -Wno-write-strings $<

%.o: %.c
	gcc $(CFS) -o $*.o $<
