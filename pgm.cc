//	Copyright (c) 2021 Matt Samudio (Albert Lea Data)  All Rights Reserved.
//	Contact information for Albert Lea Data is available at
//	http://www.albertleadata.com
//
//	This file is part of libjsmn.
//
//	libjsmn is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as
//	published by the Free Software Foundation, either version 3 of
//	the License, or (at your option) any later version.
//
//	libjsmn is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the
//	GNU Lesser General Public License along with libjsmn.
//	If not, see <http://www.gnu.org/licenses/>.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "json.h"

int main( int argc, char *argv[]);
void jsonTest01(char *);
void jsonTest02(char *,char *);

int main( int argc, char *argv[]) {
	char *arg = (argc > 1) ? argv[1] : 0L;
	char *key = (argc > 2) ? argv[2] : 0L;
//	jsonTest01(arg);
	jsonTest02(arg,key);
	return(0);
}

void jsonTest01(char *jsonfile) {
	if (jsonfile) {
		FILE *in = fopen(jsonfile,"r");
		if (in) {
			size_t len=8191;
			char *json = new char[len+1];
			memset(json,0,len+1);
			size_t rd = fread(json,1,len,in);
		//	fprintf(stderr,"DBG=>>> read %ld from %s\n",rd,jsonfile);
			if ( rd < len) {
				JSON *doc = new JSON();
				if (doc) {
					doc->load(json);
					char *out = doc->dump();
					if (out) {
						printf("%s\n",out);
						delete[] out;
					}
					delete doc; doc=0L;
				}
			} else fprintf(stderr,"ERR=>>> possible buffer overflow\n");
			if (json) delete[] json; json=0L;
			fclose(in);
		} else fprintf(stderr,"ERR=>>> %s (%s) %s\n","JSON file",jsonfile,"not found");
	} else fprintf(stderr,"ERR=>>> %s\n","JSON file name required");
	return;
}

void jsonTest02(char *jsonfile,char *key) {
	if (jsonfile && key) {
		FILE *in = fopen(jsonfile,"r");
		if (in) {
			size_t len=8191;
			char *json = new char[len+1];
			memset(json,0,len+1);
			size_t rd = fread(json,1,len,in);
		//	fprintf(stderr,"DBG=>>> read %ld from %s\n",rd,jsonfile);
			if ( rd < len) {
				JSON *doc = new JSON();
				if (doc) {
					doc->load(json);
					JNode *opt = doc->get(key);
					if (opt) {
						fprintf(stderr,"DBG=>>> %s: %s\n",key,(char *)(*opt));
					} else fprintf(stderr,"ERR=>>> %s not found\n",key);
					delete doc; doc=0L;
				}
			} else fprintf(stderr,"ERR=>>> possible buffer overflow\n");
			if (json) delete[] json; json=0L;
			fclose(in);
		} else fprintf(stderr,"ERR=>>> %s (%s) %s\n","JSON file",jsonfile,"not found");
	} else fprintf(stderr,"ERR=>>> %s\n","JSON file name required");
	return;
}
